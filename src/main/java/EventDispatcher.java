

import com.sun.istack.internal.NotNull;
import lombok.Getter;
import org.apache.commons.lang3.ClassUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter

public class EventDispatcher {
    public static final EventDispatcher INSTANCE = new EventDispatcher();
    private Map<Class<?>, List<Object>> interfaceToObjectsMap = new HashMap<>();
    private ExecutorService executorService = Executors.newFixedThreadPool(4);
    private Map<Integer, PhoneSystem> phoneSystems = new HashMap<>();

    private EventDispatcher() {
    }

    public void register(Object givenObject) {
        //if object is a PhoneSystem, add to the system list
        if(givenObject instanceof PhoneSystem) phoneSystems.put(((PhoneSystem) givenObject).id, (PhoneSystem) givenObject);

        //get the list of interfaces implemented by given object
        List<Class<?>> interfaces = ClassUtils.getAllInterfaces(givenObject.getClass());

        //
        for (Class<?> thisInterface : interfaces) {
            List<Object> objectsOfThisInterface = interfaceToObjectsMap.get(thisInterface);
            if (objectsOfThisInterface == null) {
                objectsOfThisInterface = new ArrayList<>();
            }
            objectsOfThisInterface.add(givenObject);
            interfaceToObjectsMap.put(thisInterface, objectsOfThisInterface);
        }
    }

    public <T> List<T> getAllObjectsImplementingInterface(Class<T> tClass) {
        return (List<T>) interfaceToObjectsMap.get(tClass);
    }

    public void fireEvent(@NotNull IEvent event) {
        executorService.submit(() -> {
            try {
                event.run();
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getMessage());
            }
        });
    }
}
