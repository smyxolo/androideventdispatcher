

public interface IAndroidEventListener {
    void callReceived(int caller_id, int receiver_id);
    void smsReceived(int sender, int receiver, String text);

}
