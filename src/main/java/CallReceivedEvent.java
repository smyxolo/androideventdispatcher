import java.util.List;

public class CallReceivedEvent implements IEvent {
    private int callerID, receiverID;

    public CallReceivedEvent(int callerID, int receiverID) {
        this.callerID = callerID;
        this.receiverID = receiverID;
    }

    @Override
    public void run() {
        List<IAndroidEventListener> listenerList = EventDispatcher.INSTANCE.getAllObjectsImplementingInterface(IAndroidEventListener.class);
        for (IAndroidEventListener listener : listenerList) {
            listener.callReceived(callerID, receiverID);
        }
    }
}
