

public class PhoneApp extends App implements IAppMonitorCallListener, IAndroidEventListener {

    boolean isCalling = false;
    private int connectedDeviceID = 0;

    public PhoneApp(int systemID) {
        super(systemID);
        declareIO(2, 1);
    }

    public void inlet(int number) {
        connectedDeviceID = number;
    }

    public void Call() {
        if (!isCalling) {
            outlet(0, 1);
            EventDispatcher.INSTANCE.fireEvent(new MakeCallEvent(super.getSystemID(), connectedDeviceID));
        }
    }

    public void Answer() {
        EventDispatcher.INSTANCE.fireEvent(new CallStartedEvent(connectedDeviceID, super.getSystemID()));
        isCalling = true;
    }

    public void Decline() {
        EventDispatcher.INSTANCE.fireEvent(new CallFinishedEvent(connectedDeviceID, super.getSystemID()));
        isCalling = false;
    }


    @Override
    public void callStarted(int callerId, int receiverId) {
        if (super.getSystemID() == callerId) {
            isCalling = true;
            outlet(0, 1);
        } else if (super.getSystemID() == receiverId) {
            isCalling = true;
            outlet(0, 1);
        }
    }

    @Override
    public void callFinished(int caller, int receiver) {
        if (super.getSystemID() == caller || super.getSystemID() == receiver) {
            outlet(0, 2);
        }
    }

    @Override
    public void smsReceived(int sender_id, int receiver_id, String text) {

    }

    @Override
    public void callReceived(int caller_id, int receiver_id) {
        if (receiver_id == super.getSystemID()) {
            connectedDeviceID = caller_id;
            outlet(0, 0);
        }
    }
}
