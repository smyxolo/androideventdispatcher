import java.util.List;

public class CallFinishedEvent implements IEvent {
    private int callerId;
    private int receiverId;

    public CallFinishedEvent(int callerId, int receiverId) {
        this.callerId = callerId;
        this.receiverId = receiverId;
    }

    @Override
    public void run() {
        List<IAppMonitorCallListener> listenerList = EventDispatcher.INSTANCE.getAllObjectsImplementingInterface(IAppMonitorCallListener.class);
        for (IAppMonitorCallListener listener :listenerList) {
            listener.callFinished(callerId, receiverId);
        }
    }
}
