public interface IMonitorSmsListener {
    void smsReceived(int sender_id, int receiver_id, String text);
}
