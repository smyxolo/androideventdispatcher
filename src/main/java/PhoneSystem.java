import com.cycling74.max.Atom;
import com.cycling74.max.DataTypes;
import com.cycling74.max.MaxObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter

public class PhoneSystem extends MaxObject implements IAndroidEventListener, IAppMonitorCallListener, IMonitorSmsListener {
    private boolean isCalling = false;
    public int id, connectedID;
    private List<App> appList;
    String text;

    public PhoneSystem(int id) {
        EventDispatcher.INSTANCE.register(this);
        this.id = id;
        declareAttribute("id");
        setAttr("id", id);
        declareInlets(new int[]{DataTypes.ANYTHING, DataTypes.INT, DataTypes.ANYTHING});
        System.out.println("New PhoneSystem: ID=" + getAttr("id"));
    }

    public void addApp(App app) {
        appList.add(app);
    }


    public void callReceived(int caller_id, int receiver_id) {
        if (receiver_id == this.id) {
            outlet(0, "Phone" + caller_id + " is Calling...");
            connectedID = caller_id;

        }

    }


    public void smsReceived(int sender, int receiver, String text) {
        String s = "Message from phone" + sender + ":\n" + text;
        if(receiver == this.id) outlet(0, Atom.newAtom(s));
        if(sender == this.id) outlet(0, "Your message was sent to Phone" + receiver);
    }


    public void callStarted(int callerId, int receiverId) {
        if(callerId == this.id || receiverId == this.id) {
            outlet(0, "connection with Phone" + connectedID);
            System.out.println(this.id + ": connection with Phone" + connectedID);
        }
    }


    public void callFinished(int caller, int receiver) {
        if(caller == this.id || receiver == this.id) {
            isCalling = false;
            outlet(0, "disconnected from Phone" + connectedID);
        }
    }
}
