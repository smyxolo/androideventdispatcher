import com.cycling74.max.DataTypes;

public class SmsApp extends App {

    private int connectedNumber;
    private String text;

    public SmsApp(int systemID) {
        super(systemID);
        declareInlets(new int[]{DataTypes.ANYTHING, DataTypes.INT, DataTypes.ANYTHING});
    }

    public void inlet(int numberToText){
        connectedNumber = numberToText;
    }


    public void text(String[] msg){
        StringBuilder s = new StringBuilder();
        for (String string : msg) {
            s.append(string);
            s.append(" ");
        }
        text = s.toString();
        EventDispatcher.INSTANCE.fireEvent(new TextSentEvent(super.getSystemID(), connectedNumber, text));
    }


    @Override
    public void callStarted(int thisSystemID, int connectedID) {

    }

    @Override
    public void callFinished(int thisSystemID, int connectedID) {

    }

    @Override
    public void smsReceived(int sender_id, int receiver_id, String text) {
        if(super.getSystemID() == receiver_id) {
            System.out.println("SmsApp @PhoneSystem:" + super.getSystemID() + " " +
                    "received a message from PhoneSystem:" + sender_id + text);
            outletBang(0);
        }
    }
}
