

public interface IAppMonitorCallListener {
    void callStarted(int callerId, int receiverId);
    void callFinished(int callerId, int receiverId);
}