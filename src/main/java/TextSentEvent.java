import java.util.List;

public class TextSentEvent implements IEvent {
    int sender;
    int receiver;
    String text;

    public TextSentEvent(int sender, int receiver, String text) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
    }

    @Override
    public void run() {
        List<IMonitorSmsListener> listenerList = EventDispatcher.INSTANCE.getAllObjectsImplementingInterface(IMonitorSmsListener.class);
        listenerList.forEach(listener -> listener.smsReceived(sender, receiver, text));
    }
}
