import com.cycling74.max.MaxObject;
import lombok.Getter;

@Getter
public abstract class App extends MaxObject implements IAppMonitorCallListener, IMonitorSmsListener {
    private int systemID;

    public App(int systemID) {
        EventDispatcher.INSTANCE.register(this);
        this.systemID = systemID;
        declareAttribute("systemID");
        setAttr("systemID", systemID);
        System.out.println("New App for PhoneSystem " + getAttr("systemID"));
    }

    public App() {
        EventDispatcher.INSTANCE.register(this);
    }
}
