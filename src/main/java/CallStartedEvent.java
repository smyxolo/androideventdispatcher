import java.util.List;

public class CallStartedEvent implements IEvent {
    private int thisSystemID;
    private int connectedID;

    public CallStartedEvent(int thisSystemID, int connectedID) {
        this.thisSystemID = thisSystemID;
        this.connectedID = connectedID;
    }

    @Override
    public void run() {
        List<IAppMonitorCallListener> listenerList = EventDispatcher.INSTANCE.getAllObjectsImplementingInterface(IAppMonitorCallListener.class);
        for (IAppMonitorCallListener listener: listenerList) {
            listener.callStarted(thisSystemID, connectedID);
        }
    }
}
