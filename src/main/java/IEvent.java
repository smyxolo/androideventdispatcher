

public interface IEvent extends Runnable {
    void run();
}
